import processing.serial.*; //<>// //<>//
import toxi.geom.Vec2D;
import toxi.geom.Line2D;

Serial myPort;                       // The serial port
boolean connectionWithGRBL = false;

//DRAWING
/////////////////////////////////////////////////////////////////////////
// PARAMS
String outputFolder = "./data/";
String outputFile = "bufferGCode";

int paperWidth = 700;
int paperHeight = 700;

// Paper sheet dimensions (in mm in real life)
int pW = paperWidth;
int pH = paperHeight;

// let's increase them for a bigger preview in Processing
int areaOnScreenWidth = 580;
float scaleRatio = float(areaOnScreenWidth)/float(paperWidth);
int areaOnScreenHeight = int( paperHeight*scaleRatio);
int offsetX;
int offsetY;


// Defines an output file
PrintWriter output;
boolean GCodeExported = false;

// Define pen UP and DOWN positions
float penUp = 1.0f;
float penDown = 0.0f;
boolean isPenDown = false; // used in order not to repeat unnecessarily the penDown command

// Define Feed rate (stepper motors speed)
float motorFeedSlow = 5000.0f;
float motorFeedFast = 7000.0f;

// arraylist to save lines coordinates
ArrayList<Line2D> pixelLines = new ArrayList<Line2D>();
ArrayList<Line2D> pixelLinesBlurred = new ArrayList<Line2D>();
ArrayList<Line2D> paperLines = new ArrayList<Line2D>();

// Used for reading the generated GCode
String[] lines;
int index = 0;

boolean isStreamingGcode = false;
boolean firstContact = true;

int state = 0;

char previousChar;

String Pen1 = "M4";
String Pen2 = "M3";

boolean whichPen = true;
int servoUp = 255;
int servoDown = 0;

boolean funnyInterpretationDone = false;
int minSteps = 0;
int maxSteps= 20;
int randomAmount = 30;
float rand = 0;

//IMAGES
PImage bckground;
int animalID = 0;

ArrayList <PImage> myCaptchas = new ArrayList();

void setup() {
  size(1024, 600);  // Stage size
  frameRate(25);
  background(255);
  smooth();
  stroke(1);

  //Try to connect on each Serial port available, then wait for a response.
  //If nothing, no GRBL 
  printArray(Serial.list());
  for (int i=0; i<Serial.list().length; i++)
  {
    if (connectionWithGRBL == true) break;
    try {
      myPort = new Serial(this, Serial.list()[i], 115200);
    }
    catch(Exception e) {
      e.printStackTrace();
    }
    delay(2000);
    if (i == Serial.list().length-1 && connectionWithGRBL == false)
    {
      println("No GRBL found");
      break;
    }
  }

  offsetX = ((width/2)-areaOnScreenWidth)/2 + 465;
  offsetY = (height-areaOnScreenHeight)/2;

  ellipseMode(CENTER);
  bckground = loadImage("interface.jpg");
  for (int i = 0; i<8; i++)
  {
    PImage mycaptcha = loadImage("image"+i+".jpg");
    myCaptchas.add(mycaptcha);
  }
  myPort.write("$X" + "\n");
  delay(1000);
  myPort.write("M3 S255" + "\n");
  delay(1000);
  myPort.write("M4 S255" + "\n");
  delay(1000);
  myPort.write("$H" + "\n");
  delay(10000);
  myPort.write("G10 P0 L20 X0 Y0 Z0" + "\n");
  
}

void draw() {
  background (bckground);
  image(myCaptchas.get(animalID), 64,245);
  switch (state)
  { 
  case 0:
    //////////////////////////////////////////////////
    //Drawing
    //////////////////////////////////////////////////

    //Draw page area
    noStroke();
    fill (255);
    rect (offsetX, offsetY, areaOnScreenWidth, areaOnScreenHeight);

    // the Drawing
    stroke(0);
    // Check if mous is inside the screen
    if (mousePressed == true && mouseX>offsetX+20 && mouseX<offsetX+areaOnScreenWidth-20 && mouseY>offsetY+20 && mouseY<offsetY+areaOnScreenHeight-20) {
      line(mouseX, mouseY, pmouseX, pmouseY);

      // INTO GCODE
      Line2D linePixel;
      linePixel = new Line2D(new Vec2D(pmouseX, pmouseY), new Vec2D(mouseX, mouseY));

      pixelLines.add(linePixel);
    }
    // Draw the lines on screen
    for (int i = 0; i< pixelLines.size(); i++)
    {
      Line2D currentLine = pixelLines.get(i);
      float screenX1 = currentLine.a.x;
      float screenY1 = currentLine.a.y;  
      float screenX2  = currentLine.b.x;
      float screenY2  = currentLine.b.y;
      line(screenX1, screenY1, screenX2, screenY2);
    }
    break;

  case 1:
    funnyInterpretation();
    //Draw page area

    fill (255);
    rect (offsetX, offsetY, areaOnScreenWidth, areaOnScreenHeight);
    fill (0, 125);
    rect (0,0, width, height);

    stroke(255, 0, 0);
    fill(0, 255, 255);
    // Draw the lines on screen, but in a funny way
    println(pixelLinesBlurred.size());
    for (int i = 0; i< pixelLinesBlurred.size(); i++)
    {
      Line2D currentLine = pixelLinesBlurred.get(i);
      float screenX1 = currentLine.a.x;
      float screenY1 = currentLine.a.y;  
      float screenX2  = currentLine.b.x;
      float screenY2  = currentLine.b.y;
      line(screenX1, screenY1, screenX2, screenY2);
      /*
      ellipse(screenX1, screenY1, 5, 5);
       fill (0);
       ellipse(screenX2, screenY2, 5, 5);
       */
    }

    delay(500);
    state = 2;
    break;



  case 2:
    //////////////////////////////////////////////////
    //Xport to GCODE
    //////////////////////////////////////////////////
    // Begin to write a file to store GCode lines.
    fill (0, 125);
    rect (0,0, width, height);
    output = createWriter(outputFolder + outputFile +".ngc");
    GCodeInit();
    GCodeWrite();
    state = 3;
    break;

  case 3:
    //////////////////////////////////////////////////
    //Stream GCode
    //////////////////////////////////////////////////
    fill (0, 125);
    rect (0,0, width, height);
    streamGCode();
  }
}

/////////////////////////////////////////////////////////////////////////////////////////


void streamGCode()
{
  lines = loadStrings("bufferGCode.ngc");
  myPort.write(" " + "\n");
  delay(2000);
  isStreamingGcode = true;
}


/////////////////////////////////////////////////////////////////////////////////////////
void keyPressed()
{
  if (key == 'E' || key == 'e') state = 1;
  //Homing
  if (key == 'H') myPort.write("$H" + "\n");
  //Unlock
  if (key == 'U') myPort.write("$X" + "\n");
  //Origin Y
  if (key == 'Y') myPort.write("G10 P0 L20 Y0" + "\n");
  //Origin X
  if (key == 'X') myPort.write("G10 P0 L20 X0" + "\n");
  //Origin M3 UP
  if (key == 'O')myPort.write("M3 S255" + "\n");
  //Origin M4 UP
  if (key == 'P')myPort.write("M4 S255" + "\n");
  //Origin M3 DOWN
  if (key == 'L')myPort.write("M3 S0" + "\n");
  //Origin M4 DOWN
  if (key == 'M')myPort.write("M4 S0" + "\n");

  //Enrouler le papier
  if (key == 'Q')myPort.write( "G21 G91 G0  Y500" + "\n");
  //Dérouler le papier
  if (key == 'W')myPort.write( "G21 G91 G0  Y-500" + "\n");
  
  
}

void mousePressed()
{
  println(mouseX);
  if (state == 0 && mouseX>58 && mouseX<217 && mouseY>136 && mouseY<430)
  {
   raz(); 
  }else if (state == 0 && mouseX>226 && mouseX<386 && mouseY>136 && mouseY<430)
  {
    state = 1;
  }
}

public void GCodeInit() {
  index = 0;
  System.out.println("Init");
  System.out.println("Changing Pen");
  whichPen = !whichPen;
  output.println("M3" + " " + "S" + servoUp);
  output.println("M4" + " " + "S" + servoUp);
  //output.println("( Made with Processing / Paper size: "  + pW + "x" + pH + "mm )");
  output.println("G21");
  //output.println("G0" + " " + "Z" + penUp);
  
  output.println("G0" + " " + "F" + motorFeedFast + " " + "X0.0" + " " +  "Y0.0"); 
  output.println(" ");
}

public void GCodeWrite() {

  // export GCODE file
  convertPixelLinesToPaperLines();
  for (int i=0; i<paperLines.size (); i++) {
    // iterate through the saved lines coordinates and write them to the GCode file
    Line2D currentLine = paperLines.get(i);
    if (i == 0) {
      // move from the home/origin to the first point with penUP
      output.println(thePen() + " " + "S" + servoUp);
      output.println("G0" + " " + "Z" + penUp);
      output.println("G0" + " " + "F" + motorFeedFast);
      output.println("G0" + " " + "X" + currentLine.a.x + " " + "Y" + (currentLine.a.y));          
      // this is the first line that gets drawn
      output.println(thePen() + " " + "S" + servoDown);
      output.println("G1" + " " + "Z" + penDown);
      output.println("G0" + " " + "F" + motorFeedSlow);
      isPenDown = true;
      output.println("G1" + " " + "X" + currentLine.a.x + " " + "Y" + (currentLine.a.y));
      output.println("G1" + " " + "X" + currentLine.b.x + " " + "Y" + (currentLine.b.y));
      //println(l.a.x + " " +l.a.y);
    } else {
      Line2D previousLine = paperLines.get(i-1);

      //output.println ("--");
      //output.println ("CurrentLine "+ currentLine + " AX " + currentLine.a.x + " AY " + currentLine.a.y );
      //output.println ("PreviousLine "+ previousLine + " BX " + previousLine.b.x + " BY " + previousLine.b.y );
      //output.println ("--");

      if (currentLine.a.x == previousLine.b.x && currentLine.a.y == previousLine.b.y) {
        // the two lines share a vertex, so the pen head is not lifetd
        //output.println("Line " + i + " and line " + (i-1) + " share a vertex");
        if (isPenDown != true) {
          output.println(thePen() + " " + "S" + servoDown);
          output.println("G1" + " " + "Z" + penDown);
          isPenDown = true;
        }
        output.println("G1" + " " + "X" + currentLine.a.x + " " + "Y" + (currentLine.a.y));
        output.println("G1" + " " + "X" + currentLine.b.x + " " + "Y" + (currentLine.b.y));
      } else {
        // the two lines DO NOT share a vertex, so the pen head IS RAISED
        // the pen head is quickly moved to the next vertex
        output.println(thePen() + " " + "S" + servoUp);
        output.println("G0" + " " + "Z" + penUp);
        output.println("G0" + " " + "F" + motorFeedFast);
        output.println("G1" + " " + "X" + currentLine.a.x + " " + "Y" + (currentLine.a.y));

        // the line is drawn
        output.println(thePen() + " " + "S" + servoDown);
        output.println("G0" + " " + "Z" + penDown); 
        output.println("G0" + " " + "F" + motorFeedSlow);
        output.println("G1" + " " + "X" + currentLine.a.x + " " + "Y" + (currentLine.a.y));
        output.println("G1" + " " + "X" + currentLine.b.x + " " + "Y" + (currentLine.b.y));
      }
    }
  }
  GCodeEnd();
} 



public void GCodeEnd() {
  System.out.println("End");
  // writes a footer with the end instructions for the GCode output file
  output.println(" ");
  // G0 Z90.0
  // G0 X0 Y0 => go home
  // M5 => stop spindle
  // M30 => stop execution
  output.println(thePen() + " " + "S" + servoUp);
  output.println("G0" + " " + "Z" + penUp);
  //output.println("G0 Z90.0");
  output.println("G0 X0 Y0");  
  // Each pen up
  output.println("M3" + " " + "S" + servoUp);
  output.println("M4" + " " + "S" + servoUp);
  //On remote la feuille de 30cm
  output.println( "G0  Y-300" + "\n");   
  // On redéfinit le Y =0
  output.println("G10 P0 L20 Y0" + "\n");
  //output.println("M30"); 
  // finalize the GCode text file and quits the current Processing Sketch
  output.flush();  // writes the remaining data to the file
  output.close();  // finishes the output file
  println("***************************");
  println("GCODE EXPORTED SUCCESSFULLY");
  println("***************************");
  delay(500);
}

public float fromPixelToPaperValue(float pixelValue, float scaleRatio_) {
  // normalize the pixel X,Y coordinates to the real paper sheet dimensions expressed in mm
  float paperValue = (pixelValue/scaleRatio_); 

  return paperValue;
}

public void convertPixelLinesToPaperLines() {

  for (int i=0; i<pixelLines.size (); i++) {
    // converts the coordinates from pixel to paper values, adjusting for a different origin
    Line2D currentLine = pixelLines.get(i);

    float paperX1 = fromPixelToPaperValue(currentLine.a.x-offsetX, scaleRatio);
    float paperY1 = fromPixelToPaperValue(currentLine.a.y-offsetY, scaleRatio);  
    float paperX2  = fromPixelToPaperValue(currentLine.b.x-offsetX, scaleRatio);
    float paperY2  = fromPixelToPaperValue(currentLine.b.y-offsetY, scaleRatio);
    
          // To avoid offscreen points
   // if (paperX1 <0) paperX1 = 0;
    //if (paperX1 >paperWidth) paperX1 = paperWidth;
    //if (paperX2 <0) paperX2 = 0;
    //if (paperX2 >paperWidth) paperX2 = paperWidth;
    
    Line2D linePaper = new Line2D(new Vec2D(paperX1, paperY1), new Vec2D(paperX2, paperY2));
    paperLines.add(linePaper);
  }
}

public void funnyInterpretation()
{
  if (funnyInterpretationDone == false)
  {
    for (int i=0; i<pixelLines.size (); i++) {
      // converts the coordinates from pixel to paper values, adjusting for a different origin
      Line2D currentLine = pixelLines.get(i);
      PVector vectorStart = new PVector (currentLine.a.x, currentLine.a.y);
      PVector vectorEnd = new PVector (currentLine.b.x, currentLine.b.y);

      // Get size of the line
      float distance = PVector.dist(vectorStart, vectorEnd);

      float numberOfSteps = round(distance/10)+1;

      //float numberOfSteps = round(random(minSteps, maxSteps)*2);
      for (float j=1; j<=numberOfSteps; j++)
      {
        PVector inBetween0 = new PVector();
        float amount0 = (j-1)/numberOfSteps;
        inBetween0 = PVector.lerp(vectorStart, vectorEnd, amount0);

        PVector inBetween1 = new PVector();
        float amount1 = j/numberOfSteps;
        inBetween1 = PVector.lerp(vectorStart, vectorEnd, amount1);

        if (j >1 && j<numberOfSteps)
        {
          rand = 0;
          inBetween0 = shake(inBetween0);
          inBetween1 = shake(inBetween1);
        }

        if (j >1)
        {
          Line2D testPreviousLine = pixelLinesBlurred.get(pixelLinesBlurred.size()-1);
          Vec2D testPreviousVec = testPreviousLine.b;
          Line2D linePaper = new Line2D(/*new Vec2D(inBetween0.x, inBetween0.y)*/testPreviousVec, new Vec2D(inBetween1.x, inBetween1.y));  
          pixelLinesBlurred.add(linePaper);
        } else {
          Line2D linePaper = new Line2D(new Vec2D(inBetween0.x, inBetween0.y), new Vec2D(inBetween1.x, inBetween1.y));
          pixelLinesBlurred.add(linePaper);
        }
      }
      funnyInterpretationDone = true;
    }
    // Clone pixelLinesBlurred to pixelLines
    pixelLines.clear();
    for (int j=0; j<pixelLinesBlurred.size (); j++) {
      pixelLines.add(pixelLinesBlurred.get(j)) ;
    }
  }
}

PVector shake( PVector _myVector)
{
  //GRIBOUILLIS
  //_myVectorCoord = _myVectorCoord + (random(randomAmount)-randomAmount/2);

  PVector myPVector =  _myVector;
  myPVector.x = myPVector.x + random(-rand, rand);
  myPVector.y = myPVector.y + random(-rand, rand);
  rand += random(-randomAmount, randomAmount);
  /*
  PVector myPVector =  _myVector;
   myPVector.x = myPVector.x + (randomAmount*noise(xoff))-randomAmount/2;
   myPVector.y = myPVector.y + (randomAmount*noise(yoff))-randomAmount/2;
   println((randomAmount*noise(xoff))-randomAmount/2);
   */
  return myPVector;
}


void raz()
{
  pixelLines.clear();
  paperLines.clear();
  pixelLinesBlurred.clear();
  funnyInterpretationDone = false;
}

String thePen() {
  String theChosenPen;
  if (whichPen == true)theChosenPen = Pen1;
  else {
    theChosenPen = Pen2;
  }
  return theChosenPen;
}


void serialEvent(Serial myPort) {
  // read a char from the serial port:
  char inChar = myPort.readChar();
  // GRBL Connection
  if (inChar =='G' && connectionWithGRBL == false)
  {
    connectionWithGRBL = true;
    println ("Connection with GRBL is done");
  } 
  //received an "ok"
  else if (isStreamingGcode && inChar =='k' && previousChar=='o') {
    if (index<lines.length) 
    {
      String[] command = split(lines[index], '\n');
      String joinedCommand = join(command, " ");
      println ("Command is:" + joinedCommand + " --- Index is" + index + "/" +lines.length);
      myPort.write(joinedCommand + "\n");
      index++;
      if (index == lines.length)
      {

        println ("end of stream");
        delay(3000);
        //myPort.write(0x18 + "\n");
        isStreamingGcode=false;
        delay(2000);
        state = 0;
        raz();
        
        // Changing animal
        animalID = int(random(0,myCaptchas.size()));
        
      }
    }
  } else {
    print (inChar);
  }
  previousChar = inChar;
}