

/*
  Controlling a servo position using a potentiometer (variable resistor)
  by Michal Rinott <http://people.interaction-ivrea.it/m.rinott>

  modified on 8 Nov 2013
  by Scott Fitzgerald
  http://www.arduino.cc/en/Tutorial/Knob
*/

#include <Servo.h>

Servo myservo1;  // create servo object to control a servo
Servo myservo2;  // create servo object to control a servo

int potpin = 0;  // analog pin used to connect the potentiometer
int SpindleDir, SpindleEna;
int val;

int SpindleDirPin = 2;// D2
int SpindleEnaPin = 3;// D3
int Servo1Val = 0;
int Servo2Val = 0;

void setup() {
  myservo1.attach(8); // D8
  myservo2.attach(9); // D9
  pinMode( SpindleDirPin, INPUT);
  pinMode( SpindleEnaPin, INPUT);
  Serial.begin(115200); // Debug
}

void loop() {
  SpindleDir = digitalRead(SpindleDirPin);
  SpindleEna = digitalRead(SpindleEnaPin);

//  Serial.print( " D:");
//  Serial.print(SpindleDir);
//  Serial.print( " E:");
//  Serial.print(SpindleEna);

  // Version A
//    Servo1Val = HIGH;
//    Servo2Val = HIGH;
//    if (HIGH == SpindleEna) {
//      if( HIGH == SpindleDir) {
//        Servo2Val = LOW;
//      } else {
//        Servo1Val = LOW;
//      }
//    }

  // Version B
  // Crab feel artifact risk with the one below
  if (HIGH == SpindleDir) {
    if ( HIGH == SpindleEna) {
      Servo2Val = LOW;
    } else {
      Servo2Val = HIGH;
    }
  } else if ( HIGH == SpindleEna) {
    Servo1Val = LOW;
  } else {
    Servo1Val = HIGH;
  }
//}


// MMMM High is 0 like M3 S0
val = map(Servo1Val, LOW, HIGH, 80, 0);     // scale it
myservo1.write( val);                  // sets the servo position according to the scaled value
Serial.print( " S1:");
Serial.print( val);

val = map(Servo2Val, LOW, HIGH, 100, 180);     // scale it
myservo2.write( val);                  // sets the servo position according to the scaled value
Serial.print( " S2:");
Serial.print( val);

Serial.println(" ");

delay(15);                           // waits for the servo to get there

}
